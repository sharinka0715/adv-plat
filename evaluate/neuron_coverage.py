import torch
from torch import nn
from tqdm import tqdm

from .base_evaluate import BaseEvaluate
from models.base_model import BaseModel

class NeuronCoverage(BaseEvaluate):
    """
    K-Multisection Neuron Coverage
    """

    def __init__(self, model: BaseModel, k=20, **kwargs):
        self.model = model
        self.layers = []
        self.k = k
        self._unfold_layer(self.model.module)

    def _unfold_layer(self, module):
        """
        获取模型所有层
        由于模型存在大量submodule，仅遍历表层named_module不能起到遍历所有神经元的作用
        """
        for name, submodule in module.named_children():
            sublayer = len(list(submodule.named_children()))
            if sublayer == 0:
                self.layers.append(submodule)
            else:
                self.layers.append(submodule)
                #self._unfold_layer(submodule)


    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()

        kmncov = 0
        total = 0

        tqdm_len = len(self.layers) * (len(clean_loader) + len(adv_loader))
        with tqdm(total=tqdm_len) as pbar:
            for module in self.layers:
                high, low = None, None
                # clean_loader装入“训练集”
                pbar.set_description("Processing clean loader")
                for bs in clean_loader:
                    features = []
                    def forward_hook(module, input, output):
                        features.append(output)
                    hook = module.register_forward_hook(forward_hook)

                    data = bs["data"]
                    if use_cuda:
                        data = data.cuda()
                    with torch.no_grad():
                        self.model(data)

                    hook.remove()
                    
                    # 计算high和low
                    features = torch.cat(features, dim=0)
                    num_features = features.shape[1]
                    if len(features.shape) == 4:
                        features = features.permute(0, 2, 3, 1).reshape(-1, num_features)

                    # 防止爆显存，最多只存一个batch，一个module的特征图
                    # 否则resnet152_denoise特征图在这个地方必爆
                    if high is None:
                        high = features.max(0).values.unsqueeze(0)
                        low = features.min(0).values.unsqueeze(0)
                    else:
                        _high = features.max(0).values.unsqueeze(0)
                        _low = features.min(0).values.unsqueeze(0)
                        high = torch.cat([_high, high], dim=0).max(0).values.unsqueeze(0)
                        low = torch.cat([_low, low], dim=0).min(0).values.unsqueeze(0)
                        
                    pbar.set_postfix({"high": high[0][0].item(), "low": low[0][0].item()})
                    pbar.update()

                high = high.squeeze()
                low = low.squeeze()
                space = (high - low) / self.k

                # adv_loader装入“测试集”，计算神经元覆盖率
                kmncov_set = []
                upper = 0
                lower = 0
                pbar.set_description("Processing adv loader")
                for bs in adv_loader:
                    features = []
                    def forward_hook(module, input, output):
                        features.append(output)
                    hook = module.register_forward_hook(forward_hook)

                    data = bs[0]
                    if use_cuda:
                        data = data.cuda()
                    with torch.no_grad():
                        self.model(data)

                    hook.remove()
                    
                    features = torch.cat(features, dim=0)
                    num_features = features.shape[1]
                    # 利用pytorch的broadcast机制，进行不同维度tensor之间的四则运算
                    if len(features.shape) == 4:
                        features = features.permute(0, 2, 3, 1).reshape(-1, num_features)
                    features = torch.ceil((features - low) / space)

                    
                    for i in range(num_features):
                        neuron_list = list(set(features[:, i].type(torch.LongTensor).cpu().tolist()))
                        # 计算KMNCov
                        kmncov_list = [e for e in neuron_list if e >= 1 and e <= self.k]
                        if len(kmncov_set) < num_features:
                            kmncov_set.append(set(kmncov_list))
                        else:
                            kmncov_set[i] = kmncov_set[i] | set(kmncov_list)

                        # 计算NBCov和SNACov
                        for t in neuron_list:
                            if t > self.k:
                                upper += 1
                                break

                        for t in neuron_list:
                            if t < 1:
                                lower += 1
                                break

                    pbar.set_postfix({"kmncov_len": len(kmncov_set[0])})
                    pbar.update()

                for i in range(len(kmncov_set)):
                    kmncov += len(kmncov_set[i])
                total += len(kmncov_set)

        return "{} {} {}".format(kmncov / (total * self.k), (upper + lower) / (total * 2), upper / total)