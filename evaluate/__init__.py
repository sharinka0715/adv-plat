from .base_evaluate import BaseEvaluate
from .model_task_performance import ACC, ACAC, ACTC, NTE
from .model_defense_performance import CAV, CCV, COS, CRR, CSR
from .model_structure import EBD, ENI, NSense, EBD2, NUncer
from .data_performance import ALDp, ASS, PSD
from .neuron_coverage import NeuronCoverage
from models.base_model import BaseModel

REGISTRY = {
    "ca": BaseEvaluate,
    "aaw": ACC,
    "aab": ACC,
    "acac": ACAC,
    "actc": ACTC,
    "nte": NTE,
    "cav": CAV,
    "ccv": CCV,
    "cos": COS,
    "crr": CRR,
    "csr": CSR,
    "eni": ENI,
    "nsense": NSense,
    "nuncer": NUncer,
    "aldp": ALDp,
    "ass": ASS,
    "psd": PSD,
    "ebd": EBD,
    "ebd2": EBD2,
    "ncov": NeuronCoverage
}

def evaluate_entry(method_name: str, model: BaseModel, **kwargs) -> BaseEvaluate:
    return REGISTRY[method_name](model, **kwargs)
