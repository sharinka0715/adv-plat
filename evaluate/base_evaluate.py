import torch
from torch import nn
from tqdm import tqdm

from models.base_model import BaseModel

class BaseEvaluate:
    """
    Implemented Clean Accuracy
    """
    def __init__(self, model: BaseModel, **kwargs):
        self.model = model
        
    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        count, total = 0, 0
        for bs in tqdm(clean_loader):
            data, label = bs["data"], bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                pred = torch.argmax(self.model(data), dim=1)
                count += torch.sum(pred==label).item()
                total += label.shape[0]
        acc = count / total
        return acc
