import torch
from torch import nn
from tqdm import tqdm
from torch.nn import functional as F

from .base_evaluate import BaseEvaluate
from models.base_model import BaseModel
from utils.ssim import SSIM


class ALDp(BaseEvaluate):
    """
    Average Lp Distortion
    """

    def __init__(self, model: BaseModel, p, **kwargs):
        self.model = model
        self.p = p

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        count, total = 0, 0
        adv_loader = iter(adv_loader)
        for bs in tqdm(clean_loader):
            data_adv = next(adv_loader)[0]
            data = bs["data"]
            label = bs["label"]
            if use_cuda:
                data_adv, data, label = data.cuda(), data_adv.cuda(), label.cuda()

            with torch.no_grad():
                pred = torch.argmax(self.model(data_adv), dim=1)
                for i in range(data.shape[0]):
                    if pred[i] != label[i]:
                        gap = torch.norm(data_adv[i] - data[i], p=self.p)
                        x = torch.norm(data[i], p=self.p)
                        count += (gap / x).item()
                        total += 1
        
        aldp = count / total
        return aldp


class ASS(BaseEvaluate):
    """
    Average Structural Similarity
    """

    def __init__(self, model: BaseModel, **kwargs):
        self.model = model

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        ssim_loss = SSIM()
        ssim, total = 0, 0
        adv_loader = iter(adv_loader)
        for bs in tqdm(clean_loader):
            data_adv = next(adv_loader)[0]
            data = bs["data"]
            label = bs["label"]
            if use_cuda:
                data_adv, data, label = data.cuda(), data_adv.cuda(), label.cuda()

            with torch.no_grad():
                pred = torch.argmax(self.model(data_adv), dim=1)
                for i in range(data.shape[0]):
                    if pred[i] != label[i]:
                        ssim += ssim_loss(data_adv[i:i+1], data[i:i+1]).item()
                        total += 1

        ass = ssim / total
        return ass


class PSD(BaseEvaluate):
    """
    Perturbation Sensitivity Distance
    """

    def __init__(self, model: BaseModel, **kwargs):
        self.model = model

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        psd, total = 0, 0
        adv_loader = iter(adv_loader)

        for bs in tqdm(clean_loader):
            data_adv = next(adv_loader)[0]
            data = bs["data"]
            label = bs["label"]
            if use_cuda:
                data_adv, data, label = data.cuda(), data_adv.cuda(), label.cuda()

            with torch.no_grad():
                pred = torch.argmax(self.model(data_adv), dim=1)
                for i in range(data.shape[0]):
                    if pred[i] != label[i]:
                        total += 1
                        sen_total = 0
                        delta = torch.abs(data_adv[i] - data[i])
                        img_pad = nn.functional.pad(data[i].unsqueeze(0), (1, 1, 1, 1), "reflect")
                        # for c in range(delta.shape[0]):
                        #     for m in range(delta.shape[1]):
                        #         for n in range(delta.shape[2]):
                        #             sen = torch.std(img_pad[c, m:m+3, n:n+3].reshape(-1), unbiased=False).item()
                        #             if sen < 1e-4:
                        #                 sen = 1e-4
                        #             sen_total += delta[c][m][n] / sen

                        # 使用卷积加速PSD计算
                        kernel = (torch.ones((3, 1, 3, 3)) / 9).cuda()
                        img_pad_square = img_pad ** 2
                        ex2 = F.conv2d(img_pad_square, kernel, groups=3)
                        e2x = F.conv2d(img_pad, kernel, groups=3) ** 2
                        std = (ex2 - e2x).squeeze()
                        stdmax = std.max().item()
                        std = torch.sqrt(torch.clamp(std, 1e-8, stdmax))
                        psd += torch.mean(delta / std).item()

        psd = psd / total
        return psd