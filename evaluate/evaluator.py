import torch
from torch import nn
from torch.utils.data import DataLoader, TensorDataset

from . import evaluate_entry
from datasets import dataset_entry


class Evaluator:
    def __init__(self, model, cfg) -> None:
        self.cfg = cfg
        self.model = model
        if cfg.use_cuda:
            self.model.load_state_dict(torch.load(self.cfg.ckpt_path))
        else:
            self.model.load_state_dict(torch.load(self.cfg.ckpt_path, map_location=torch.device('cpu')))

        if "kwargs" not in self.cfg:
            self.cfg["kwargs"] = {}
        self.method = evaluate_entry(self.cfg.method, self.model, **self.cfg.kwargs)

        self.clean_dataset = None
        self.clean_loader = None

        if "clean_dataset" in self.cfg:
            self.clean_dataset = dataset_entry(self.cfg.clean_dataset, **self.cfg.clean_dataset_kwargs)
            self.clean_loader = DataLoader(self.clean_dataset, batch_size=self.cfg.batch_size, shuffle=False, num_workers=self.cfg.num_workers)

        self.adv_dataset = None
        self.adv_loader = None
        
        if "adv_data_path" in self.cfg:
            adv_data = torch.load(self.cfg.adv_data_path)
            adv_label = torch.load(self.cfg.adv_label_path)
            self.adv_dataset = TensorDataset(adv_data, adv_label)
            self.adv_loader = DataLoader(self.adv_dataset, batch_size=self.cfg.batch_size, shuffle=False, num_workers=self.cfg.num_workers)
            

    def evaluate(self):
        metric = self.method.eval(self.clean_loader, self.adv_loader, self.cfg.use_cuda)
        with open("result.log", "a") as fp:
            fp.write("{}\n{}\n{}: {}\n\n".format(self.cfg.ckpt_path, self.cfg.adv_data_path, self.cfg.method, metric))
        print(self.cfg.method, metric)
