"""
输入数据格式说明：
corruption_count代表输入数据有多少个不同的corruption，不同强度和不同类别的corruption都算不同的corruption
需要保证len(clean_loader) * corruption_count = len(adv_loader)
同一个corruption的图片连在一起，同一张图的不同corruption分开
"""
import torch
from torch import nn
from tqdm import tqdm

from .base_evaluate import BaseEvaluate
from models.base_model import BaseModel


class mCE(BaseEvaluate):
    """
    Mean Corruption Error
    """
    def __init__(self, model: BaseModel, corruption_count, **kwargs):
        self.model = model
        self.corruption_count = corruption_count

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        adv_loader = iter(adv_loader)
        mce = 0
        for _ in self.corruption_count:
            count, total = 0, 0
            for bs in tqdm(clean_loader):
                data = next(adv_loader)[0]
                label = bs["label"]
                if use_cuda:
                    data, label = data.cuda(), label.cuda()

                with torch.no_grad():
                    pred = torch.argmax(self.model(data), dim=1)
                    count += torch.sum(pred!=label).item()
                    total += label.shape[0]
                
            mce += count / total

        return mce / self.corruption_count


class RmCE(BaseEvaluate):
    """
    Relative Mean Corruption Error
    """
    def __init__(self, model: BaseModel, corruption_count, **kwargs):
        self.model = model
        self.corruption_count = corruption_count

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        adv_loader = iter(adv_loader)
        rmce = 0
        for _ in self.corruption_count:
            count, total = 0, 0
            count_clean = 0
            for bs in tqdm(clean_loader):
                data = next(adv_loader)[0]
                data_clean = bs["data"]
                label = bs["label"]
                if use_cuda:
                    data, label = data.cuda(), label.cuda()
                    data_clean = data_clean.cuda()

                with torch.no_grad():
                    pred = torch.argmax(self.model(data), dim=1)
                    pred_clean = torch.argmax(self.model(data_clean), dim=1)
                    count += torch.sum(pred!=label).item()
                    count_clean += torch.sum(pred_clean!=label).item()
                    total += label.shape[0]
                
            rmce += (count - count_clean) / total

        return rmce / self.corruption_count


#TODO: MFR