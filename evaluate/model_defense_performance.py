import torch
from torch import nn
from tqdm import tqdm
import scipy.stats

from .base_evaluate import BaseEvaluate
from models.base_model import BaseModel
from models import model_entry


class CAV(BaseEvaluate):
    """
    Classification Accuracy Variance
    """

    def __init__(self, model: BaseModel, vanilla_model, vanilla_model_kwargs, **kwargs):
        self.model = model
        self.vanilla_model = model_entry(vanilla_model, **vanilla_model_kwargs)
        if vanilla_model_kwargs.parallel:
            self.vanilla_model = nn.DataParallel(self.vanilla_model)
        self.vanilla_model.load_state_dict(torch.load(vanilla_model_kwargs.ckpt_path))

    def eval(self, clean_loader, adv_loader, use_cuda):
        if use_cuda:
            self.vanilla_model.cuda()

        self.model.eval()
        self.vanilla_model.eval()
        adv_loader = iter(adv_loader)

        count, total = 0, 0
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                pred = torch.argmax(self.model(data), dim=1)
                pred_vanilla = torch.argmax(self.vanilla_model(data), dim=1)
                count += (torch.sum(pred == label) - torch.sum(pred_vanilla == label)).item()
                total += label.shape[0]

        cav = count / total

        return cav


class CRR(BaseEvaluate):
    """
    Classification Rectify Ratio
    """

    def __init__(self, model: BaseModel, vanilla_model, vanilla_model_kwargs, **kwargs):
        self.model = model
        self.vanilla_model = model_entry(vanilla_model, **vanilla_model_kwargs)
        if vanilla_model_kwargs.parallel:
            self.vanilla_model = nn.DataParallel(self.vanilla_model)
        self.vanilla_model.load_state_dict(torch.load(vanilla_model_kwargs.ckpt_path))

    def eval(self, clean_loader, adv_loader, use_cuda):
        if use_cuda:
            self.vanilla_model.cuda()

        self.model.eval()
        self.vanilla_model.eval()
        adv_loader = iter(adv_loader)

        count, total = 0, 0
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():  # 加速，不做back propogation
                pred = torch.argmax(self.model(data), dim=1)
                pred_vanilla = torch.argmax(self.vanilla_model(data), dim=1)

                # use vectorization to speed up
                count += torch.sum(torch.mul(pred == label, pred_vanilla != label)).item()
                total += label.shape[0]

        crr = count / total

        return crr


class CSR(BaseEvaluate):
    """
    Classification Sacrifice Ratio
    """

    def __init__(self, model: BaseModel, vanilla_model, vanilla_model_kwargs, **kwargs):
        self.model = model
        self.vanilla_model = model_entry(vanilla_model, **vanilla_model_kwargs)
        if vanilla_model_kwargs.parallel:
            self.vanilla_model = nn.DataParallel(self.vanilla_model)
        self.vanilla_model.load_state_dict(torch.load(vanilla_model_kwargs.ckpt_path))

    def eval(self, clean_loader, adv_loader, use_cuda):
        if use_cuda:
            self.vanilla_model.cuda()

        self.model.eval()
        self.vanilla_model.eval()
        adv_loader = iter(adv_loader)

        count, total = 0, 0
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():  # 加速，不做back propogation
                pred = torch.argmax(self.model(data), dim=1)
                pred_vanilla = torch.argmax(self.vanilla_model(data), dim=1)

                # use vectorization to speed up
                count += torch.sum(torch.mul(pred != label, pred_vanilla == label)).item()
                total += label.shape[0]

        csr = count / total

        return csr

class CCV(BaseEvaluate):
    """
    Classification Confidence Variance
    """

    def __init__(self, model: BaseModel, vanilla_model, vanilla_model_kwargs, **kwargs):
        self.model = model
        self.vanilla_model = model_entry(vanilla_model, **vanilla_model_kwargs)
        if vanilla_model_kwargs.parallel:
            self.vanilla_model = nn.DataParallel(self.vanilla_model)
        self.vanilla_model.load_state_dict(torch.load(vanilla_model_kwargs.ckpt_path))

    def eval(self, clean_loader, adv_loader, use_cuda):
        if use_cuda:
            self.vanilla_model.cuda()

        self.model.eval()
        self.vanilla_model.eval()
        adv_loader = iter(adv_loader)
        # 找出所有能够被正确识别的样本。
        # P是F的softmax层输出，F(x)=arg maxP(x)
        count, n = 0, 0
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():  # 加速，不做back propogation
                pred = self.model(data)
                pred_vanilla = self.vanilla_model(data)
                p = torch.max(nn.functional.softmax(pred, dim=1), dim=1).values
                p_vanilla = torch.max(nn.functional.softmax(pred_vanilla, dim=1), dim=1).values
                pred = torch.argmax(pred, dim=1)  # 预测结果1
                pred_vanilla = torch.argmax(pred_vanilla, dim=1)  # 预测结果2

                # use vectorization to speed up
                example_correct = torch.mul(pred == label, pred_vanilla == label)
                # 只有两个模型都预测正确时对应位置是1，否则是0
                count += torch.sum(torch.mul(torch.abs(p - p_vanilla), example_correct)).item()
                n += torch.sum(example_correct).item()

        ccv = count / n

        return ccv

class COS(BaseEvaluate):
    """
    Classification Output Stability
    """

    def __init__(self, model: BaseModel, vanilla_model, vanilla_model_kwargs, **kwargs):
        self.model = model
        self.vanilla_model = model_entry(vanilla_model, **vanilla_model_kwargs)
        if vanilla_model_kwargs.parallel:
            self.vanilla_model = nn.DataParallel(self.vanilla_model)
        self.vanilla_model.load_state_dict(torch.load(vanilla_model_kwargs.ckpt_path))

    def eval(self, clean_loader, adv_loader, use_cuda):
        if use_cuda:
            self.vanilla_model.cuda()

        self.model.eval()
        self.vanilla_model.eval()
        adv_loader = iter(adv_loader)
        # 找出所有能够被正确识别的样本。
        # P是F的softmax层输出，F(x)=arg maxP(x)
        count, n = 0, 0
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            def JSD(p, q):
                mid = (p + q) / 2
                return scipy.stats.entropy(p, mid) / 2 + scipy.stats.entropy(q, mid) / 2

            with torch.no_grad():  # 加速，不做back propogation
                pred = self.model(data)
                pred_vanilla = self.vanilla_model(data)
                p = nn.functional.softmax(pred, dim=1)
                p_vanilla = nn.functional.softmax(pred_vanilla, dim=1)
                pred = torch.argmax(pred, dim=1)  # 预测结果1
                pred_vanilla = torch.argmax(pred_vanilla, dim=1)  # 预测结果2
                jsd = torch.zeros((p.shape[0], ))

                for i in range(p.shape[0]):
                    jsd[i] = JSD(p[i].cpu(), p_vanilla[i].cpu())
                jsd = jsd.cuda()

                # use vectorization to speed up
                example_correct = torch.mul(pred == label, pred_vanilla == label)
                # 只有两个模型都预测正确时对应位置是1，否则是0
                count += torch.sum(torch.mul(jsd, example_correct)).item()
                n += torch.sum(example_correct).item()

        cos = count / n

        return cos
