import torch
from torch import nn
from tqdm import tqdm

from .base_evaluate import BaseEvaluate
from models.base_model import BaseModel


class ACC(BaseEvaluate):
    """
    Adversarial Accuracy on White-box / Black-box Attacks
    """
    def __init__(self, model: BaseModel, **kwargs):
        self.model = model
        
    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        count, total = 0, 0
        adv_loader = iter(adv_loader)
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                pred = torch.argmax(self.model(data), dim=1)
                count += torch.sum(pred==label).item()
                total += label.shape[0]
        acc = count / total
        return acc


class ACAC(BaseEvaluate):
    """
    Average Confidence of Adversarial Class
    """
    def __init__(self, model: BaseModel, **kwargs):
        self.model = model
        
    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        conf, total = 0, 0
        adv_loader = iter(adv_loader)
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                pred = nn.functional.softmax(self.model(data), dim=1)
                pred_label = torch.argmax(pred, dim=1)
                for i in range(label.shape[0]):
                    # untargeted attack successful
                    if pred_label[i] != label[i]:
                        conf += pred[i][pred_label[i]].item()
                        total += 1
                
        acac = conf / total
        return acac


class ACTC(BaseEvaluate):
    """
    Average Confidence of True Class
    """
    def __init__(self, model: BaseModel, **kwargs):
        self.model = model
        
    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        conf, total = 0, 0
        adv_loader = iter(adv_loader)
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                pred = nn.functional.softmax(self.model(data), dim=1)
                pred_label = torch.argmax(pred, dim=1)
                for i in range(label.shape[0]):
                    # untargeted attack successful
                    if pred_label[i] != label[i]:
                        total += 1
                        conf += pred[i][label[i]].item()
                
        actc = conf / total
        return actc


class NTE(BaseEvaluate):
    """
    Noise Tolerance Estimation
    """
    def __init__(self, model: BaseModel, **kwargs):
        self.model = model
        
    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        conf, total = 0, 0
        adv_loader = iter(adv_loader)
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                pred = nn.functional.softmax(self.model(data), dim=1)
                pred_label = torch.argmax(pred, dim=1)
                for i in range(label.shape[0]):
                    # untargeted attack successful
                    if pred_label[i] != label[i]:
                        total += 1
                        conf += (pred[i][pred_label[i]] - pred[i][label[i]]).item()
                
        nte = conf / total
        return nte