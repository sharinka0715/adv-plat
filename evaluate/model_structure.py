import math
import torch
from torch import nn
from tqdm import tqdm

from .base_evaluate import BaseEvaluate
from models.base_model import BaseModel


class EBD(BaseEvaluate):
    """
    Empirical Boundary Distance
    """
    def __init__(self, model: BaseModel, input_shape=[3, 32, 32], num_directions=1000, **kwargs):
        self.model = model
        mat = torch.randn((input_shape[0] * input_shape[1] * input_shape[2], num_directions))
        q, _ = torch.linalg.qr(mat)
        self.vectors = q.T.reshape((num_directions, *input_shape)) * math.sqrt(input_shape[0] * input_shape[1] * input_shape[2])
        self.arange = torch.arange(0, 128, 2) / 128

    def _measure_one_image(self, data, label):
        distances = []
        for index in range(self.vectors.shape[0]):
            flag = 0
            for t in self.arange:
                data_delta = torch.clamp(data + self.vectors[index] * t, 0, 1)
                pred = torch.argmax(self.model(data_delta.unsqueeze(0)), dim=1)[0]
                if pred != label:
                    distances.append(t.item())
                    flag = 1
                    break
            if flag == 0:
                distances.append(1)
        return distances
        

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()

        ebd, total = 0, 0
        if use_cuda:
            self.vectors = self.vectors.cuda()
            self.arange = self.arange.cuda()

        for bs in tqdm(clean_loader):
            data = bs["data"]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                for i in range(data.shape[0]):
                    distances = self._measure_one_image(data[i], label[i])
                    with open("ebd_dir.log", "a") as fp:
                        fp.write(str(distances) + "\n")
                    ebd += min(distances)
                    total += 1

        return ebd / total

                
class EBD2(BaseEvaluate):
    """
    Empirical Boundary Distance - 2
    """
    def __init__(self, model: BaseModel, alpha=0.0005, max_iter=100, **kwargs):
        self.model = model
        self.alpha = alpha
        self.max_iter = max_iter

    def _measure_one_image(self, data, label):
        data = data.clone().detach().unsqueeze(0)
        label = label.unsqueeze(0)

        for i in range(self.max_iter):
            data = data.detach()
            data.requires_grad_(True)
            pred = self.model(data)
            y = torch.argmax(pred, dim=1)[0]
            if y != label:
                return i

            loss = nn.CrossEntropyLoss()(pred, label)
            loss.backward()
            grad_sign = data.grad.sign()
            data = torch.clamp(data + self.alpha * grad_sign, 0, 1)
        return self.max_iter

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()

        ebd2, total = 0, 0
        for bs in tqdm(clean_loader):
            data = bs["data"]
            label = bs["label"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()

            for i in range(data.shape[0]):
                ebd2 += self._measure_one_image(data[i], label[i])
                total += 1

        return ebd2 / total


class ENI(BaseEvaluate):
    """
    ε-Empirical Noise Insensitivity
    Recommend batch size to be 1.
    """

    def __init__(self, model: BaseModel, **kwargs):
        self.model = model

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        adv_loader = iter(adv_loader)

        eni, total = 0, 0
        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            data_clean = bs["data"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()
                data_clean = data_clean.cuda()

            with torch.no_grad():  
                loss = nn.functional.cross_entropy(self.model(data), label, reduction='sum')
                loss_clean = nn.functional.cross_entropy(self.model(data_clean), label, reduction='sum')
                linf = torch.norm(data - data_clean, float('inf'))
                eni += (torch.abs(loss - loss_clean) / linf).item()
                total += data.shape[0]

        return eni / total
                

class NSense(BaseEvaluate):
    """
    Neuron Sensitivity
    """

    def __init__(self, model: BaseModel, register_name=[], **kwargs):
        self.model = model
        self.register_name = register_name

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        adv_loader = iter(adv_loader)

        nsense = {}
        total = 0

        for bs in tqdm(clean_loader):
            data = next(adv_loader)[0]
            label = bs["label"]
            data_clean = bs["data"]
            if use_cuda:
                data, label = data.cuda(), label.cuda()
                data_clean = data_clean.cuda()

            # register hook
            features = []
            hooks = []
            def forward_hook(module, input, output):
                features.append(output)

            for name in self.register_name:
                hook = eval("self.model.module." + name).register_forward_hook(forward_hook)
                hooks.append(hook)

            with torch.no_grad():
                self.model(data)
                self.model(data_clean)
                num_features = len(self.register_name)

                for i in range(num_features):
                    if self.register_name[i] not in nsense:
                        nsense[self.register_name[i]] = torch.norm(features[i] - features[i+num_features], p=1) / features[i].shape[1]
                    else:
                        nsense[self.register_name[i]] += torch.norm(features[i] - features[i+num_features], p=1) / features[i].shape[1]
                total += data.shape[0]
                
            for hook in hooks:
                hook.remove()
        
        for key in nsense:
            nsense[key] /= total
                
        return nsense


class NUncer(BaseEvaluate):
    """
    Neuron Uncertainty
    """

    def __init__(self, model: BaseModel, register_name=[], **kwargs):
        self.model = model
        self.register_name = register_name

    def eval(self, clean_loader, adv_loader, use_cuda):
        self.model.eval()
        adv_loader = iter(adv_loader)

        nuncer = {}
        total = 0

        for bs in tqdm(clean_loader):
            label = bs["label"]
            data_clean = bs["data"]
            if use_cuda:
                label = label.cuda()
                data_clean = data_clean.cuda()

            # register hook
            features = []
            hooks = []
            def forward_hook(module, input, output):
                features.append(output)

            for name in self.register_name:
                hook = eval("self.model.module." + name).register_forward_hook(forward_hook)
                hooks.append(hook)

            with torch.no_grad():
                self.model(data_clean)
                num_features = len(self.register_name)

                for i in range(num_features):
                    if self.register_name[i] not in nuncer:
                        nuncer[self.register_name[i]] = torch.var(features[i].view(-1))
                    else:
                        nuncer[self.register_name[i]] += torch.var(features[i].view(-1))
                total += data_clean.shape[0]

            for hook in hooks:  
                hook.remove()
        
        for key in nuncer:
            nuncer[key] /= total
                
        return nuncer