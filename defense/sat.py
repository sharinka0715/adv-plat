import torch
from torch import nn
from advertorch.attacks import GradientSignAttack

from .base_defense import BaseDefense
from models.base_model import BaseModel


class SAT(BaseDefense):
    def __init__(self, model: BaseModel, criterion="CrossEntropyLoss()", eps=0.01, adv_ratio=1):
        self.model = model
        self.criterion = eval("nn." + criterion)
        self.adv_ratio = adv_ratio

        self._FGSM = GradientSignAttack(self.model, self.criterion, eps=eps)
    
    def cal_loss(self, data, label):
        """
        cal_loss方法接收原始数据和真实标签，返回需要训练时反向传播的loss
        第一个返回值用于backward，第二个返回值用于print
        """
        self.model.eval()
        data_adv = self._FGSM.perturb(data, label).detach()
        self.model.train()

        loss_adv = self.criterion(self.model(data_adv), label)
        loss_ori = self.criterion(self.model(data), label)

        loss = (loss_ori + self.adv_ratio * loss_adv) / (1 + self.adv_ratio)

        return loss, {"loss_ori": loss_ori.item(), "loss_adv": loss_adv.item(), "loss_total": loss.item()}
