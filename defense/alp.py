import torch
from torch import nn
from advertorch.attacks import LinfPGDAttack

from .base_defense import BaseDefense
from models.base_model import BaseModel

class ALP(BaseDefense):
    def __init__(self, model: BaseModel, criterion="CrossEntropyLoss()", eps=0.01, eps_iter=0.001, num_steps=40, alp_ratio=1):
        self.model = model
        self.criterion = eval("nn." + criterion)
        self.alp_ratio = alp_ratio

        self._PGD = LinfPGDAttack(self.model, self.criterion, eps=eps, nb_iter=num_steps, eps_iter=eps_iter)
    
    def cal_loss(self, data, label):
        """
        cal_loss方法接收原始数据和真实标签，返回需要训练时反向传播的loss
        第一个返回值用于backward，第二个返回值用于print
        """
        self.model.eval()
        data_adv = self._PGD.perturb(data, label).detach()
        self.model.train()

        logits_adv = self.model(data_adv)
        logits_ori = self.model(data)
        loss_adv = self.criterion(logits_adv, label)
        loss_ori = self.criterion(logits_ori, label)

        loss_alp = nn.MSELoss()(logits_adv, logits_ori)

        loss = (loss_adv + loss_ori) / 2 + self.alp_ratio * loss_alp

        return loss, {"loss_adv": loss_adv.item(), "loss_ori": loss_ori.item(), "loss_alp": loss_alp.item(), "loss_total": loss.item()}
