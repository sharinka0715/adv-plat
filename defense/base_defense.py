import torch
from torch import nn

from models.base_model import BaseModel

class BaseDefense:
    def __init__(self, model: BaseModel, criterion="CrossEntropyLoss()", **kwargs):
        self.model = model
        self.criterion = eval("nn." + criterion)
    
    def cal_loss(self, data, label):
        """
        cal_loss方法接收原始数据和真实标签，返回需要训练时反向传播的loss
        第一个返回值用于backward，第二个返回值用于print
        """
        pred = self.model(data)
        loss = self.criterion(pred, label)
        return loss, {"train_loss": loss.item()}
