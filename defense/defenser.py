import os
import time
import torch
from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter

from models.base_model import BaseModel
from datasets import dataset_entry
from . import defense_entry
from utils.optimizer import optimizer_entry
from utils.scheduler import scheduler_entry

class Defenser:
    def __init__(self, model: BaseModel, cfg) -> None:
        self.cfg = cfg
        self.model = model

        self.method = defense_entry(self.cfg.method, self.model, **self.cfg.kwargs)
        self.optimizer = optimizer_entry(self.cfg.optimizer, self.model.parameters(), **self.cfg.optimizer_kwargs)
        self.scheduler = scheduler_entry(self.cfg.scheduler, self.optimizer, **self.cfg.scheduler_kwargs)

        self.dataset = dataset_entry(self.cfg.dataset, **self.cfg.dataset_kwargs)
        self.val_dataset = dataset_entry(self.cfg.val_dataset, **self.cfg.val_dataset_kwargs)
        self.loader = DataLoader(self.dataset, batch_size=self.cfg.batch_size, shuffle=True, num_workers=self.cfg.num_workers)
        self.val_loader = DataLoader(self.val_dataset, batch_size=self.cfg.batch_size, shuffle=False, num_workers=self.cfg.num_workers)

        # 创建tensorboard log文件夹和model ckpt文件夹
        self.log_path = os.path.join(self.cfg.exp_path, "log")
        self.ckpt_path = os.path.join(self.cfg.exp_path, "ckpt")
        if not os.path.isdir(self.log_path):
            os.mkdir(self.log_path)
        if not os.path.isdir(self.ckpt_path):
            os.mkdir(self.ckpt_path)
        self.writer = SummaryWriter(self.log_path)


    def _train(self, bs):
        self.model.train()
        data, label = bs["data"], bs["label"]
        if self.cfg.use_cuda:
            data, label = data.cuda(), label.cuda()
        loss, loss_dict = self.method.cal_loss(data, label)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        return loss_dict


    def _eval(self):
        self.model.eval()
        count, total = 0, 0
        for bs in self.val_loader:
            data, label = bs["data"], bs["label"]
            if self.cfg.use_cuda:
                data, label = data.cuda(), label.cuda()

            with torch.no_grad():
                pred = torch.argmax(self.model(data), dim=1)
                count += torch.sum(pred==label).item()
                total += label.shape[0]
        acc = count / total
        
        return acc
        

    def defense(self):
        t = time.time()
        for epoch in range(self.cfg.epoches):
            for (bi, bs) in enumerate(self.loader):
                global_step = len(self.loader) * epoch + bi
                loss_dict = self._train(bs)
                if global_step % self.cfg.tensorboard_step == 0:
                    if self.cfg.use_cuda:
                        torch.cuda.synchronize()
                    print("[Time: {:.2f}][Epoch: {}/{}, Batch: {}/{}]".format(time.time() - t, epoch+1, self.cfg.epoches, bi+1, len(self.loader)), loss_dict)
                    self.writer.add_scalars("loss", loss_dict, global_step)
                    t = time.time()
                
                if global_step % self.cfg.eval_step == 0:
                    acc = self._eval()
                    print("[Time: {:.2f}][Epoch: {}/{}, Batch: {}/{}]".format(time.time() - t, epoch+1, self.cfg.epoches, bi+1, len(self.loader)), "accuracy:", acc)
                    self.writer.add_scalar("accuracy", acc, global_step)
                    t = time.time()

                if global_step % self.cfg.save_step == 0:
                    save_path = os.path.join(self.ckpt_path, "model_step{}.pth".format(global_step))
                    torch.save(self.model.state_dict(), save_path)
                    print("[Time: {:.2f}][Epoch: {}/{}, Batch: {}/{}]".format(time.time() - t, epoch+1, self.cfg.epoches, bi+1, len(self.loader)), "save model to", save_path)
                    t = time.time()

            self.scheduler.step()

        print("Save final model checkpoint to {} ......".format(self.ckpt_path))  
        save_path = os.path.join(self.ckpt_path, "model_final.pth")
        torch.save(self.model.state_dict(), save_path)
            

            