import torch
from torch import nn
import torch.nn.functional as F
from advertorch.attacks import LinfPGDAttack

from .base_defense import BaseDefense
from models.base_model import BaseModel


class TRADES(BaseDefense):
    def __init__(self, model: BaseModel, step_size=0.003, eps=0.03, num_steps=40, beta=3.0):
        self.model = model
        self.step_size = step_size
        self.eps = eps
        self.num_steps = num_steps
        self.beta = beta

    def _trades_inf_loss(self, x_natural, y, step_size=0.003, epsilon=0.031, perturb_steps=10, beta=1.0):
        # define KL-loss
        criterion_kl = nn.KLDivLoss(reduction="sum")

        self.model.eval()
        batch_size = len(x_natural)
        # generate adversarial example
        x_adv = x_natural.detach() + 0.001 * torch.randn(x_natural.shape).cuda().detach()
        for _ in range(perturb_steps):
            x_adv.requires_grad_(True)
            with torch.enable_grad():
                loss_kl = criterion_kl(F.log_softmax(self.model(x_adv), dim=1), 
                                       F.softmax(self.model(x_natural), dim=1))
            grad = torch.autograd.grad(loss_kl, [x_adv])[0]
            x_adv = x_adv.detach() + step_size * torch.sign(grad.detach())
            x_adv = torch.min(torch.max(x_adv, x_natural - epsilon), x_natural + epsilon)
            x_adv = torch.clamp(x_adv, 0.0, 1.0)

        self.model.train()

        x_adv = torch.clamp(x_adv, 0.0, 1.0).detach()
        x_adv.requires_grad_(False)

        # calculate robust loss
        logits = self.model(x_natural)
        loss_natural = F.cross_entropy(logits, y)
        loss_robust = (1.0 / batch_size) * criterion_kl(F.log_softmax(self.model(x_adv), dim=1),
                                                        F.softmax(self.model(x_natural), dim=1))
        loss = loss_natural + beta * loss_robust
        return loss, loss_natural, loss_robust
    
    def cal_loss(self, data, label):
        """
        cal_loss方法接收原始数据和真实标签，返回需要训练时反向传播的loss
        第一个返回值用于backward，第二个返回值用于print
        """
        loss, loss_natural, loss_robust = self._trades_inf_loss(data, label, self.step_size, self.eps, self.num_steps, self.beta)

        return loss, {"loss_natural": loss_natural.item(), "loss_robust": loss_robust.item(), "loss_total": loss.item()}
