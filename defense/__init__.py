from .base_defense import BaseDefense
from .pat import PAT
from .sat import SAT
from .trades import TRADES
from .alp import ALP
from models.base_model import BaseModel

REGISTRY = {
    "clean": BaseDefense,
    "pat": PAT,
    "sat": SAT,
    "trades": TRADES,
    "alp": ALP
}

def defense_entry(method_name: str, model: BaseModel, **kwargs) -> BaseDefense:
    return REGISTRY[method_name](model, **kwargs)
