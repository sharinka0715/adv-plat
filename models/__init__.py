from models.base_model import BaseModel
from .vgg import *
from .wideresnet import *
from .resnet import *
from .resnet_denoise import *
from .resnet_denoise2 import *
from .inception import *

REGISTRY = {
    "vgg16": vgg16_bn,
    "vgg16_bn": vgg16_bn,
    "wrn28": wrn28,
    "resnet50": resnet50,
    "resnet50_imagenet": resnet50_imagenet,
    "resnet50_denoise": resnet50_denoise,
    "resnet50_denoise_imagenet": resnet50_denoise_imagenet,
    "inception_v3": inception_v3,
    "inception_v3_imagenet": inception_v3_imagenet,
    "resnet152_denoise": resnet152_denoise,
    "resnet152_denoise_imagenet": resnet152_denoise_imagenet
}

def model_entry(model_name: str, **kwargs) -> BaseModel:
    return REGISTRY[model_name](**kwargs)