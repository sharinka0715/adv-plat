# AdvPlat

#### 介绍
基于Python的对抗攻防平台

#### 依赖
* Python >= 3.6
* torch==1.8.1
* torchvision==0.9.1
* tqdm
* scipy
* numpy
* opencv-python
* tensorboardX
* easydict
* pyyaml
* advertorch
* foolbox（for BA）
* cleverhans (for SPSA)
* git+https://github.com/fra31/auto-attack (for AutoAttack)

#### 快速上手

```
python main.py --config experiments/你的实验/
```

#### 使用说明

* 程序的入口为`main.py`，程序具体执行的内容由`config.yaml`文件控制。
* 需要建立自己的实验时，请在`experiments`文件夹下建立实验文件夹，并在其中放入`config.yaml`文件，文件具体格式请参考template
* 如果不想进行防御/攻击/评测，请在`config.yaml`中删除相应的配置部分。
* 在`experiments/你的实验/log`文件夹下存储了TensorBoard events文件，可在此目录下调用TensorBoard查看。防御算法存储了loss和acc，攻击算法存了攻击后的图像预览。
* 防御算法的结果在`experiments/你的实验/ckpt`文件夹下，保存了模型的参数
* 攻击算法的结果在`experiments/你的实验/adv_data`文件夹下，保存了攻击后得到的对抗样本（Tensor形式）
* 评测算法的结果直接通过print输出

