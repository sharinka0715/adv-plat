import os
import sys
import yaml
import torch
import random
import argparse
import numpy as np

from torch import nn
from pprint import pprint
from tqdm import tqdm
from easydict import EasyDict

from models import model_entry
from defense.defenser import Defenser
from attack.attacker import Attacker
from evaluate.evaluator import Evaluator


def fix_seed(seed=17373051):
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)


if __name__ == "__main__":
    fix_seed(17373051)

    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, help="config dir for advplat", required=True)
    args = parser.parse_args()
    config_path = os.path.join(args.config, "config.yaml")
    if not os.path.isfile(config_path):
        raise IOError
    
    with open(config_path, "r") as fp:
        cfg = EasyDict(yaml.load(fp, Loader=yaml.FullLoader))

    if cfg.use_cuda:
        print("CUDA is available:", torch.cuda.is_available())
        if not torch.cuda.is_available():
            print("Warning! CUDA is not supported on your device! Set use_cuda=False.")
            cfg.use_cuda = False
            cfg.parallel = False
        else:
            print("CUDA visible device count:", torch.cuda.device_count())

    pprint(cfg)

    model = model_entry(cfg.model, **cfg.model_kwargs)
    if cfg.use_cuda:
        model = model.cuda()
    if cfg.parallel:
        model = nn.DataParallel(model)
    
    if "defense" in cfg.keys():
        print("===============Phase Defense===============")
        cfg.defense.use_cuda = cfg.use_cuda
        cfg.defense.exp_path = args.config
        defenser = Defenser(model, cfg.defense)
        defenser.defense()

    if "attack" in cfg.keys():
        print("===============Phase Attack================")
        cfg.attack.use_cuda = cfg.use_cuda
        cfg.attack.exp_path = args.config
        attacker = Attacker(model, cfg.attack)
        attacker.attack()   
    
    if "evaluate" in cfg.keys():
        print("===============Phase Evaluate==============")
        cfg.evaluate.use_cuda = cfg.use_cuda
        cfg.evaluate.exp_path = args.config
        evaluator = Evaluator(model, cfg.evaluate)
        evaluator.evaluate()
        