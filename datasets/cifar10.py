import os
import pickle
import torch
import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms as TF
from torchvision import utils


class Cifar10Dataset(Dataset):
    """
    Cifar-10 dataset contains 60,000 images, each has 32x32x3 shape.
    50000 for train, 10000 for test.
    10 classes, label in 0~9.
    TENSOR: (B, C, H, W), RGB format, each pixel in 0~1.
    """
    def __init__(self, dataset_root="./datasets/cifar10/", phase="train", transform=None, **kwargs):
        self.data = []
        self.labels = []
        self.transform = []
        if transform:
            for elm in transform:
                self.transform.append(eval("TF." + elm))
        self.transform = TF.Compose(self.transform)

        if phase == "eval":
            self.data = torch.load(os.path.join(dataset_root, "cifar10_eval_data.pth"))
            self.labels = torch.load(os.path.join(dataset_root, "cifar10_eval_label.pth"))
        else:
            if phase == "train":
                for i in range(1, 6):
                    with open(os.path.join(dataset_root, "data_batch_{}".format(i)), "rb") as fp:
                        dict = pickle.load(fp, encoding='bytes')
                    self.data.append(dict[b'data'])
                    self.labels.extend(dict[b'labels'])
            elif phase == "test":
                with open(os.path.join(dataset_root, "test_batch"), "rb") as fp:
                    dict = pickle.load(fp, encoding='bytes')
                self.data.append(dict[b'data'])
                self.labels.extend(dict[b'labels'])

            self.data = np.concatenate(self.data, axis=0)
            self.data = self.data.reshape((-1, 3, 32, 32))

            self.data = torch.Tensor(self.data) / 255
            self.labels = torch.LongTensor(self.labels)

    def __len__(self):
        return self.labels.shape[0]

    def __getitem__(self, index: int):
        data = self.data[index]
        label = self.labels[index]

        if self.transform:
            data = self.transform(data)

        sample = {"data": data, "label": label}
        return sample
        
    def make_grid(self):
        utils.save_image(utils.make_grid(self.data[:9], nrow=3), "tmp.png")
        
if __name__ == "__main__":
    Cifar10Dataset().make_grid()