import os
from PIL import Image
import csv
import torch
import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms as TF
from torchvision import utils


IMG_END = [
    "jpg", "png", "tiff"
]


def make_dataset(root):
    def sort_as_num(elm):
        return int(elm.split("/")[-1].split(".")[0])
    ls = os.listdir(root)
    ret_list = []
    for elm in ls:
        if elm.split(".")[-1] in IMG_END:
            ret_list.append(os.path.join(root, elm))
    ret_list.sort(key=sort_as_num)  
    return ret_list

            
class ImagenetDataset(Dataset):
    """
    Imagenet test dataset contains 5000 images, each has 500x500x3 shape.
    1000 classes, label in 0~999.
    TENSOR: (B, C, H, W), RGB format, each pixel in 0~1.
    """
    def __init__(self, dataset_root="./datasets/imagenet/", label_path="./datasets/imagenet/test_label.csv", transform=None, **kwargs):
        self.data_path = make_dataset(dataset_root)
        with open(label_path, "r") as fp:
            reader = list(csv.reader(fp))[1:]
        self.label = dict(reader)
        self.transform = []
        if transform:
            for elm in transform:
                self.transform.append(eval("TF." + elm))
        self.transform = TF.Compose(self.transform)
                
        
    def __len__(self):
        return len(self.data_path)
    
    
    def __getitem__(self, index):
        image_path = self.data_path[index]
        image_name = image_path.split("/")[-1]
        label = int(self.label[image_name])
        data = TF.functional.to_tensor(Image.open(image_path))

        if self.transform:
            data = self.transform(data)

        sample = {"data": data, "label": label}
        return sample
