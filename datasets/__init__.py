from torch.utils.data import Dataset
from .cifar10 import Cifar10Dataset
from .imagenet import ImagenetDataset
from .svhn import SVHNDataset

REGISTRY = {
    "cifar10": Cifar10Dataset,
    "imagenet": ImagenetDataset,
    "svhn": SVHNDataset
}

def dataset_entry(dataset_name: str, **kwargs) -> Dataset:
    return REGISTRY[dataset_name](**kwargs)