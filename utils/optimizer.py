from torch import optim

REGISTRY = {
    "SGD": optim.SGD,
    "Adam": optim.Adam
}

def optimizer_entry(optimizer_name, params, **kwargs) -> optim.Optimizer:
    return REGISTRY[optimizer_name](params, **kwargs)