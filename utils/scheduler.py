from torch.optim import lr_scheduler

REGISTRY = {
    "StepLR": lr_scheduler.StepLR
}

def scheduler_entry(scheduler_name, optimizer, **kwargs):
    return REGISTRY[scheduler_name](optimizer, **kwargs)