from io import BytesIO
import torch
from torch import nn
from PIL import Image
import numpy as np
from torchvision.transforms import functional as TFF

from .base_attack import BaseAttack
from models.base_model import BaseModel


class JPEGCompression(BaseAttack):
    def __init__(self, model: BaseModel, level=95):
        self.level = level
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        data = data.clone().detach()
        for i in range(data.shape[0]):
            f = BytesIO()
            img = TFF.to_pil_image(data[i])
            img.save(f, "jpeg", quality=self.level)
            img = Image.open(f)
            data[i] = TFF.to_tensor(img)

        return data
            

