import torch
from torch import nn
try:
    from cleverhans.torch.attacks.spsa import spsa
except ImportError:
    print("Unable to import cleverhans! SPSA is unavailable.")

from .base_attack import BaseAttack
from models.base_model import BaseModel


class SPSA(BaseAttack):
    def __init__(self, model: BaseModel, eps=0.03, num_iter=100, lr=0.01, **kwargs):
        self.model = model

        self.eps = eps
        self.num_iter = num_iter
        self.lr = lr

    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        self.model.eval()
        return spsa(self.model, data, self.eps, self.num_iter, clip_min=0, clip_max=1, y=label, learning_rate=self.lr)


