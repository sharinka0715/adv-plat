import torch
from torch import nn
from advertorch.attacks import CarliniWagnerL2Attack

from .base_attack import BaseAttack
from models.base_model import BaseModel


def clip_l2_norm(cln_img, adv_img, eps):
    noise = adv_img - cln_img
    if torch.sqrt(torch.sum(noise**2)).item() > eps:
        clip_noise = noise * eps / torch.sqrt(torch.sum(noise**2))
        clip_adv = cln_img + clip_noise
        return clip_adv
    else:
        return adv_img

class CW2(BaseAttack):
    def __init__(self, model: BaseModel, eps=0.5, lr=0.01, num_classes=10, confidence=0, binary_search_steps=9, max_iter=10000, targeted=False):
        self.model = model
        self.eps = eps

        self._CW2 = CarliniWagnerL2Attack(self.model, num_classes, confidence, targeted, lr, binary_search_steps, max_iter)
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        self.model.eval()
        data_adv = self._CW2.perturb(data, label)

        for i in range(data_adv.shape[0]):
            data_adv[i] = clip_l2_norm(data[i], data_adv[i], self.eps)
        return data_adv


