import torch
import numpy as np
from torch import nn
from torch.functional import Tensor
import tensorflow as tf
try:
    from ares.model.pytorch_wrapper import pytorch_classifier_with_logits
    from ares.attack.nattack import NAttack as NA
    from ares.loss.cross_entropy import CrossEntropyLoss
except ImportError:
    print("Unable to import ares! NAttack is unavailable.")

from .base_attack import BaseAttack
from models.base_model import BaseModel


@pytorch_classifier_with_logits(n_class=10, x_min=0.0, x_max=1.0, x_shape=[32, 32, 3], x_dtype=tf.float32, y_dtype=tf.int32)
class AresModel(nn.Module):
    def __init__(self):
        super().__init__()

    def load(self, model):
        self.model = model
        
    def forward(self, x):
        x = x.transpose(1, 2).transpose(1, 3).contiguous()
        return self.model(x).cpu()


class NAttack(BaseAttack):
    def __init__(self, model: BaseModel,  **kwargs):
        model.eval()

        self.model = AresModel()
        self.model.load(model)

        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.50
        config.gpu_options.allow_growth = True
        session = tf.Session(config=config)

        self._NAttack = NA(model=self.model, loss=CrossEntropyLoss(self.model), goal='ut', distance_metric='l_inf', session=session, samples_per_draw=300, samples_batch_size=25)
        self._NAttack.config(magnitude=0.03, max_queries=300*600, sigma=0.01, lr=0.008)
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        adv = torch.zeros(data.shape)
        for i in range(data.shape[0]):
            x = self._NAttack.attack(data[i].permute(1, 2, 0).cpu(), label[i].cpu())
            if isinstance(x, np.ndarray):
                x = torch.from_numpy(x)
            adv[i] = x.permute(2, 0, 1)

        return adv.cuda()


