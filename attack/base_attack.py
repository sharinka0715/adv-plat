import torch
from torch import nn

class BaseAttack:
    def __init__(self, model, **kwargs):
        self.model = model
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        return data
