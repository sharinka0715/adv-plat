import torch
from torch import nn
try:
    from autoattack import AutoAttack as AA
except ImportError:
    print("Unable to import autoattack! AutoAttack is unavailable.")

from .base_attack import BaseAttack
from models.base_model import BaseModel


class AutoAttack(BaseAttack):
    def __init__(self, model: BaseModel, eps=0.5, norm="Linf", **kwargs):
        self.model = model

        self._AA = AA(self.model, norm=norm, eps=eps, verbose=False)
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        self.model.eval()
        dict_adv = self._AA.run_standard_evaluation(data, label, bs=label.shape[0])
        return dict_adv


