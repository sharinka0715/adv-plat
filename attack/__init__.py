from .base_attack import BaseAttack
from .fgsm import FGSM
from .pgd import L1PGD, L2PGD, LinfPGD
from .cw2 import CW2
from .autoattack import AutoAttack
from .ba import BA
from .spsa import SPSA
from .nattack import NAttack
from .jpeg_compression import JPEGCompression
from models.base_model import BaseModel

REGISTRY = {
    "clean": BaseAttack,
    "fgsm": FGSM,
    "l1pgd": L1PGD,
    "l2pgd": L2PGD,
    "linfpgd": LinfPGD,
    "cw2": CW2,
    "autoattack": AutoAttack,
    "ba": BA,
    "spsa": SPSA,
    "nattack": NAttack,
    "jpeg": JPEGCompression
}

def attack_entry(method_name: str, model: BaseModel, **kwargs) -> BaseAttack:
    return REGISTRY[method_name](model, **kwargs)
