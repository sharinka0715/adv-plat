import torch
from torch import nn

from .base_attack import BaseAttack
from models.base_model import BaseModel

class FGSM(BaseAttack):
    def __init__(self, model: BaseModel, eps=0.01, targeted=False, criterion="CrossEntropyLoss()"):
        self.model = model
        self.criterion = eval("nn." + criterion)
        self.eps = eps
        self.targeted = targeted
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        self.model.eval()
        data.requires_grad_(True)

        pred = self.model(data)
        
        if self.targeted:
            loss = - self.criterion(pred, label)
        else:
            loss = self.criterion(pred, label)

        loss.backward()
        grad_sign = data.grad.sign()
        data_adv = torch.clamp(data + self.eps * grad_sign, 0, 1)

        return data_adv.detach()

