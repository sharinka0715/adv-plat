import torch
from torch import nn
try:
    import foolbox as fb
    from foolbox.attacks import BoundaryAttack
except ImportError:
    print("Unable to import foolbox! Boundary Attack is unavailable.")

from .base_attack import BaseAttack
from models.base_model import BaseModel


class BA(BaseAttack):
    def __init__(self, model: BaseModel, eps=0.5, steps=25000, spherical_step=0.01, source_step=0.01, **kwargs):
        model.eval()
        self.model = fb.PyTorchModel(model, bounds=(0, 1))
        self.eps = eps

        self._BA = BoundaryAttack(steps=steps, spherical_step=spherical_step, source_step=source_step)
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        return self._BA(self.model, data, label, epsilons=self.eps)[0]


