from advertorch.attacks.iterative_projected_gradient import SparseL1DescentAttack
import torch
from torch import nn
from advertorch.attacks import SparseL1DescentAttack, L2PGDAttack, LinfPGDAttack

from .base_attack import BaseAttack
from models.base_model import BaseModel

class L1PGD(BaseAttack):
    def __init__(self, model: BaseModel, eps=12, eps_iter=0.05, num_steps=50, l1_sparsity=0.95, targeted=False, **kwargs):
        self.model = model

        self._PGD = SparseL1DescentAttack(self.model, eps=eps, nb_iter=num_steps, eps_iter=eps_iter, targeted=targeted, l1_sparsity=l1_sparsity)
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        self.model.eval()
        return self._PGD.perturb(data, label)

class L2PGD(BaseAttack):
    def __init__(self, model: BaseModel, eps=0.5, eps_iter=0.05, num_steps=50, targeted=False, **kwargs):
        self.model = model

        self._PGD = L2PGDAttack(self.model, eps=eps, nb_iter=num_steps, eps_iter=eps_iter, targeted=targeted)
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        self.model.eval()
        return self._PGD.perturb(data, label)

class LinfPGD(BaseAttack):
    def __init__(self, model: BaseModel, eps=0.03, eps_iter=0.003, num_steps=40, targeted=False, **kwargs):
        self.model = model

        self._PGD = LinfPGDAttack(self.model, eps=eps, nb_iter=num_steps, eps_iter=eps_iter, targeted=targeted)
    
    def perturb(self, data, label):
        """
        perturb方法接收原始数据和目标/非目标标签，返回扰动后的图片
        """
        self.model.eval()
        return self._PGD.perturb(data, label)

