import os
import torch
from tqdm import tqdm
from torch import nn
from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter

from models.base_model import BaseModel
from datasets import dataset_entry
from . import attack_entry

class Attacker:
    def __init__(self, model: BaseModel, cfg) -> None:
        self.cfg = cfg
        self.model = model

        if cfg.use_cuda:
            self.model.load_state_dict(torch.load(self.cfg.ckpt_path))
        else:
            self.model.load_state_dict(torch.load(self.cfg.ckpt_path, map_location=torch.device('cpu')))

        if "kwargs" not in self.cfg:
            self.cfg["kwargs"] = {}
        self.method = attack_entry(self.cfg.method, self.model, **self.cfg.kwargs)
        self.dataset = dataset_entry(self.cfg.dataset, **self.cfg.dataset_kwargs)
        self.loader = DataLoader(self.dataset, batch_size=self.cfg.batch_size, shuffle=False, num_workers=self.cfg.num_workers)

        # 创建tensorboard log文件夹和对抗样本存储文件夹
        self.log_path = os.path.join(self.cfg.exp_path, "log")
        if not os.path.isdir(self.log_path):
            os.mkdir(self.log_path)
        self.writer = SummaryWriter(self.log_path)

        self.adv_path = os.path.join(self.cfg.exp_path, "adv_data")
        if not os.path.isdir(self.adv_path):
            os.mkdir(self.adv_path)

    def attack(self):
        adv_data = []
        adv_label = []
        for (bi, bs) in enumerate(tqdm(self.loader)):
            data, label = bs["data"], bs["label"]
            if self.cfg.use_cuda:
                data, label = data.cuda(), label.cuda()
            data_adv = self.method.perturb(data, label)

            adv_data.append(data_adv.clone().cpu())
            adv_label.append(label.clone().cpu())

            if self.cfg.tensorboard_step > 0 and bi % self.cfg.tensorboard_step == 0:
                image_pair = torch.cat((data[0:1], data_adv[0:1]), dim=0)
                self.writer.add_images("{}_image_pairs".format(self.cfg.method), image_pair, bi)

        print("Save adversarial examples to {} ......".format(self.adv_path))
        adv_data = torch.cat(adv_data, dim=0)
        save_path = os.path.join(self.adv_path, "{}_data.pth".format(self.cfg.method))
        torch.save(adv_data, save_path)
        adv_label = torch.cat(adv_label, dim=0)
        save_path = os.path.join(self.adv_path, "{}_label.pth".format(self.cfg.method))
        torch.save(adv_label, save_path)