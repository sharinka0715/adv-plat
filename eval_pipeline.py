import copy
import torch
import random
import numpy as np

from torch import nn
from pprint import pprint
from eval_config import generate_cfg

from models import model_entry
from evaluate.evaluator import Evaluator

def fix_seed(seed=17373051):
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)


if __name__ == "__main__":
    fix_seed(17373051)

    test_models = [
        "cifar10_wrn28_clean", 
        "cifar10_wrn28_pat", 
        "cifar10_wrn28_sat", 
        "cifar10_wrn28_trades", 
        "cifar10_wrn28_alp", 
        "svhn_vgg16_clean", 
        "svhn_vgg16_pat", 
        "svhn_vgg16_sat", 
        "svhn_vgg16_trades", 
        "svhn_vgg16_alp", 
        "imagenet_resnet152_denoise", 
        "imagenet_resnet50_clean", 
        "imagenet_resnet50_pat", 
        "imagenet_resnet50_alp", 
        "imagenet_resnet50_trades", 
    ]
    test_models = [e.split("_") for e in test_models]
    print(test_models)

    for dataset, model, defense in test_models:
        cfg = generate_cfg(dataset, model, defense)
        if cfg.use_cuda:
            print("CUDA is available:", torch.cuda.is_available())
            if not torch.cuda.is_available():
                print("Warning! CUDA is not supported on your device! Set use_cuda=False.")
                cfg.use_cuda = False
                cfg.parallel = False
            else:
                print("CUDA visible device count:", torch.cuda.device_count())

        pprint(cfg)

        model = model_entry(cfg.model, **cfg.model_kwargs)
        if cfg.use_cuda:
            model = model.cuda()
        if cfg.parallel:
            model = nn.DataParallel(model)

        print("===============Phase Evaluate==============")
        for method in cfg.evaluate.methods:
            for attack in cfg.evaluate.attacks:
                cfg_cp = copy.deepcopy(cfg)
                cfg_cp.evaluate.use_cuda = cfg.use_cuda
                del cfg_cp.evaluate["methods"]
                del cfg_cp.evaluate["attacks"]
                cfg_cp.evaluate.update(method)
                cfg_cp.evaluate.update(attack)
                pprint(cfg_cp)
                evaluator = Evaluator(model, cfg_cp.evaluate)
                evaluator.evaluate()
    